import csv;

class Vertex:
    def __init__(self, vertexID, title, address, postalCode):
        self.vertexID = vertexID; # The line number from the CSV file
        self.title = title;
        self.address = address;
        self.postalCode = postalCode;
    
    # toString() function, for debugging
    def __str__(self):
        return ("id: %d, title: %s, address: %s, postalCode: %d" % (self.vertexID, self.title, self.address, self.postalCode) );
        
class VertexHelper:
    def __init__(self, csvFilePath):
        self.__vertexList = [];
        self.__csvFilePath = csvFilePath;
        self.__loadVertexList();
    
    # Load the CSV file and populate the vertices list
    # Time complexity: O(n), n=number of lines in CSV file
    # Space complexity: O(n), n=number of lines in CSV file, Python List    
    def __loadVertexList(self):
    
        with open(self.__csvFilePath) as vertexListCSVFile: 
            csvReader = csv.reader(vertexListCSVFile, delimiter=",", quotechar="\"");   
            
            firstLine = True; 
            for row in csvReader:
                if firstLine == True: # skip the column names
                    firstLine = False;
                    continue;
                else:                                
                    v = Vertex(int(row[0]), row[1], row[2], int(row[3]));
                    self.__vertexList.append(v);

    # Time complexity: O(n), n=length of vertexList, Python List    
    def addressSearch(self, address):
        # Search the list for the address, return the item
        for item in self.__vertexList:
            if item.address == address:
                return item;
                
        # Return None if not found
        return None;
        
    def getVertexListLength(self):
        return len(self.__vertexList);
        
    def getVertexByID(self, vertexID):
        # vertice ids start at 1, not 0
        return self.__vertexList[vertexID - 1];
