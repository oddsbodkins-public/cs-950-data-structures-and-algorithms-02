# Class: Data Structures and Algorithms II - C950

# Python Version 3.11.2
# Operating System: Debian 12 (Codename: Bookworm, Testing) Linux x86_64
# Linux Kernel 6.1.20-2

import os;
from datetime import timedelta, timezone, datetime;

from Package import Package, PackageHelper;
from Vertex import Vertex, VertexHelper;    
from DistanceHelper import DistanceHelper;
from Truck import Truck;

class DeliverySimulation:

    def __init__(self):
        packageCSVFilePath = os.path.join(".", "project-data", "package-list.csv"); # for loading files, Windows has different file path structure than Linux / MacOS
        self.__packageHelper = PackageHelper(packageCSVFilePath);
        
        distanceTableCSVFilePath = os.path.join(".", "project-data", "simplified-distance-table.csv"); # for loading files, Windows has different file path structure than Linux / MacOS
        self.__distanceHelper = DistanceHelper(distanceTableCSVFilePath);
        
        vertexListCSVFilePath = os.path.join(".", "project-data", "vertex-list.csv"); # for loading files, Windows has different file path structure than Linux / MacOS
        self.__vertexHelper = VertexHelper(vertexListCSVFilePath);
        
        self.__packageHelper.updatePackageVertices(self.__vertexHelper); # Search package address to get vertex id

        # Create delivery trucks
        wguHubVertex = self.__vertexHelper.addressSearch("4001 South 700 East"); # Hub, WGU Warehouse

        self.__truck01 = Truck(1, "John Doe", wguHubVertex); # Truck 01, Driver A, Starts at Hub
        self.__truck02 = Truck(2, "Bill Wes", wguHubVertex); # Truck 02, Driver B, Starts at Hub

        # flags for tracking special instructions
        self.__haveDelayedPackagesArrived = False;
        self.__hasPackageWithWrongAddressBeenFixed = False;
        
    # Main logic, loads packages, delivers packages, checks on delays, etc.        
    def run(self, delayExecution=-1):
        # Starting time: "Drivers leave the hub no earlier than 8:00 a.m."
        nowDateTime = datetime.utcnow().replace(tzinfo=timezone.utc);
        todayDateString = nowDateTime.strftime("%Y-%m-%d");
        currentDateTime = datetime.strptime("%s 08:00:00" % (todayDateString), "%Y-%m-%d %H:%M:%S" );

        # Manually load trucks

        # These packages have early deadlines or special instructions
        onTimeEarlyDeadlines = [1, 13, 14, 15, 16, 19, 20, 29, 30, 31, 34, 37, 40];
        packagesThatShareAddressWithAbove = [7, 21, 39]; # be efficient, load truck to maximum 16 packages and packages that share addresses to save trips
        combinedPackageList = onTimeEarlyDeadlines + packagesThatShareAddressWithAbove; 

        # Trip 01        
        # On-time packages with early deadlines and extras
        
        for packageID in combinedPackageList:
            p = self.__packageHelper.hashMap.search(packageID);
            self.__truck01.loadPackage(p, currentDateTime, delayExecution);
        print(""); # Add newline, improve console formatting.
        
        
        self.deliverPackages(self.__truck01, delayExecution);
        totalTime = self.__truck01.getLastTripTotalTimeTraveled();
        currentDateTime += timedelta(days=0, seconds=totalTime);
        print(""); # Add newline, improve console formatting.

        # special instruction packages
        dealyedPackagesWithEarlyDeadline = [6, 25];

        # Trip 02
        # Delayed packages with early deadlines. Should be available after Trip 01
        if self.__haveDelayedPackagesArrived: # check if packages have arrived
            for packageID in dealyedPackagesWithEarlyDeadline:
                p = self.__packageHelper.hashMap.search(packageID);
                self.__truck01.loadPackage(p, currentDateTime, delayExecution);
            print(""); # Add newline, makes display look nicer

            self.deliverPackages(self.__truck01, delayExecution);
            totalTime = self.__truck01.getLastTripTotalTimeTraveled();
            currentDateTime += timedelta(days=0, seconds=totalTime);

        delayedPackagesWithoutEarlyDeadline = [28, 32];
        wrongAddressPackage = [9];
        packagesThatMustBeOnTruck02 = [3, 18, 36, 38];
        packagesThatShareAddressWithAbove = [2, 5, 8, 26, 27, 33, 35]; # be efficient, load truck to maximum 16 packages and packages that share addresses to save trips
        extraPackageToMakeFullLoad = [4];
        combinedPackageList = delayedPackagesWithoutEarlyDeadline + wrongAddressPackage + packagesThatMustBeOnTruck02 + packagesThatShareAddressWithAbove + extraPackageToMakeFullLoad;
        
        # Trip 03, switch to truck 02, some packages require it in special instructions
        if self.__hasPackageWithWrongAddressBeenFixed: # check if package with wrong address has been fixed
            for packageID in combinedPackageList:
                p = self.__packageHelper.hashMap.search(packageID);
                self.__truck02.loadPackage(p, currentDateTime, delayExecution);
            print(""); # Add newline, makes display look nicer

            self.deliverPackages(self.__truck02, delayExecution);
            totalTime = self.__truck02.getLastTripTotalTimeTraveled();
            currentDateTime += timedelta(days=0, seconds=totalTime);
        
        # Trip 04, finish the remaining packages, no further special instruction
        remainingPackages = [10, 11, 12, 17, 22, 23, 24];
        for packageID in remainingPackages:
            p = self.__packageHelper.hashMap.search(packageID);
            self.__truck02.loadPackage(p, currentDateTime, delayExecution);
        print(""); # Add newline, makes display look nicer

        self.deliverPackages(self.__truck02, delayExecution);
        totalTime = self.__truck02.getLastTripTotalTimeTraveled();
        currentDateTime += timedelta(days=0, seconds=totalTime);

        print("Finished all deliveries at %s." % (currentDateTime) );
        
        print(self.__truck01.getTripsSummaryString());
        print(self.__truck02.getTripsSummaryString());
                
        totalDistance = self.__truck01.getTripTotalDistance() + self.__truck02.getTripTotalDistance();
        print("All trucks and trips total distance: %f miles.\nObjective: (%f <= 140.0): %s" % (totalDistance, totalDistance, totalDistance <= 140.0) );
        
    # Make an adjaceny matrix for each requested vertex in the list. Ex: {1: 1.0, 5: 4.5, ...}
    # Return the nearest neighbor vertex
    # Time complexity: O(n), n = the number of vertices in the graph list
    # Space complexity O(n), n = the number of vertices in the graph list
    def getNearestNeighborVertex(self, fromVertex, vertexList):
        adjacenyMatrix = {};
        smallestDistance = float('inf');
        closestVertex = None;

        for vertexID in vertexList:
            distance = self.__distanceHelper.getDistance(fromVertex.vertexID, vertexID); # O(1)
            
            # Find the shortest path, the closet vertex id
            # Do not compare a vertex to itself! From A -> A = 0.0, always is the shortest distance.
            if distance < smallestDistance and vertexID != fromVertex.vertexID:
                smallestDistance = distance;
                closestVertex = self.__vertexHelper.getVertexByID(vertexID);
            
            adjacenyMatrix[vertexID] = distance; # save the distance to the dictionary / adjaceny matrix

        return closestVertex;

    # This function is for handling the special instructions. 1. The packages delayed on flights until 09:05 2. The package with the incorrect address
    def checkDelayedPackages(self, currentDateTime):
        nowDateTime = datetime.utcnow().replace(tzinfo=timezone.utc);
        todayDateString = nowDateTime.strftime("%Y-%m-%d");
        
        delayedDateTime = datetime.strptime("%s 09:05:00" % (todayDateString), "%Y-%m-%d %H:%M:%S" );
        updatedAddressDateTime = datetime.strptime("%s 10:20:00" % (todayDateString), "%Y-%m-%d %H:%M:%S" );
        
        if currentDateTime >= delayedDateTime and self.__haveDelayedPackagesArrived == False:
            print("\nSHIPPING UPDATE: Packages with id: 6, 25, 28, 32 that were delayed have arrived.\n");
            self.__haveDelayedPackagesArrived = True; # update flag
            
        if currentDateTime >= updatedAddressDateTime and self.__hasPackageWithWrongAddressBeenFixed == False:
            print("\nSHIPPING UPDATE: Package id: 9 has a new address.\n");
            p = self.__packageHelper.hashMap.search(9);
            
            # update package address and vertex
            p.setLocation("410 S State St", "Salt Lake City", "UT", 84111);
            v = self.__vertexHelper.addressSearch("410 S State St");
            p.setVertex(v);        
            
            self.__hasPackageWithWrongAddressBeenFixed = True; # update flag
                    
    # Trucks drive from hub to package addresses and then return
    # Time complexity O(n^2)  
    def deliverPackages(self, truck, delayExecution=-1):
        verticesToVisit = truck.getVerticesToVisit(); # O(1)
        lastDateTime = None;
        
        while len(verticesToVisit) != 0: # O(n)
            currentVertex = truck.getCurrentVertex();        
            # O(n)
            nearestVertex = self.getNearestNeighborVertex(currentVertex, verticesToVisit); # makes an adjaceny matrix and returns the vertex with the smallest distance

            distance = self.__distanceHelper.getDistance(currentVertex.vertexID, nearestVertex.vertexID); # O(1)
            
            # get all the packages that match this vertex (will be delivered to the same address)
            packageList = truck.getPackagesAtVertex(nearestVertex.vertexID); # O(1)
            
            # no extra distance is covered, if there are multiple packages for the same address        
            for package in packageList:        
                lastDateTime = truck.deliverPackage(package, distance, delayExecution); # O(1)
            
            # update list, avoid infinite loop
            verticesToVisit = truck.getVerticesToVisit(); # O(1)
            
            # helper function to check on package delays and special instructions
            if self.__haveDelayedPackagesArrived == False or self.__hasPackageWithWrongAddressBeenFixed == False:
                self.checkDelayedPackages(lastDateTime);

        # after delivering loaded packages, return to hub
        wguHubVertex = self.__vertexHelper.addressSearch("4001 South 700 East"); # Hub, WGU Warehouse, O(a)
        distance = self.__distanceHelper.getDistance(wguHubVertex.vertexID, truck.getCurrentVertex().vertexID); # O(1)
        return truck.returnToHub(wguHubVertex, distance, delayExecution); # O(1)

    def getPackageHelper(self):
        return self.__packageHelper;

def main():
    print("Welcome to the C950 Class Project Program.\n");
    
    timeDelaySimulation = 0.03; # time.sleep(...) argument is in seconds, 0.001 = 1 millisecond
    
    exitMenuLoop = False;
    d = DeliverySimulation();
    
    # First loop for user menu
    while exitMenuLoop == False:
    
        print("Menu");
        print("----------------------------");
        print("1. Begin delivery simulation");
        print("2. Exit\n");
        
        userChoice = -1;
        try:
            userChoice = int(input("Your selection: "));
        except:
            print("Invalid selection.\n");
            
        if userChoice == 1:
            print("\n");
            d.run(timeDelaySimulation);            
            exitMenuLoop = True;
            
        elif userChoice == 2:
            exitMenuLoop = True;
            return;
        else:
            print("Invalid selection.\n");


    exitMenuLoop = False; # reset loop flag

    # Second loop for user menu
    while exitMenuLoop == False:

        print("\n\nSecond Menu");
        print("------------------------------");
        print("1. Look at package information");
        print("2. Exit\n");
        
        userChoice = -1;
        try:
            userChoice = int(input("Your selection: "));
        except:
            print("Invalid selection.\n");
            
        if userChoice == 1:

            packageHelper = d.getPackageHelper();
            print("\n\n--------------------------------");
            print("This screen displays the status of all packages at a requested time.");
            requestedHourAndMinute = input("Enter a time in 24 hour format (Ex: \"09:30\"): ");
            print("\n");

            nowDateTime = datetime.utcnow().replace(tzinfo=timezone.utc);
            todayDateString = nowDateTime.strftime("%Y-%m-%d");
            
            requestedDateTime = datetime.strptime("%s %s:00" % (todayDateString, requestedHourAndMinute), "%Y-%m-%d %H:%M:%S" );    
            
            for i in range(packageHelper.hashMap.getNumberOfItems() + 1):
                if i > 0: # package ids are one-indexed, not zero
                    package = packageHelper.hashMap.search(i);
                    print(package.getPackageStringAtDateTime(requestedDateTime));
            
            
        elif userChoice == 2:
            exitMenuLoop = True;
            return;
        else:
            print("Invalid selection.\n");   
    
main();
