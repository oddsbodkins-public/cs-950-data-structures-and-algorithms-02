from time import sleep;
from datetime import timedelta;

from Package import Package;
from Queue import Node, Queue;

# Class for tracking trips 
# A trip is 1. Start at hub 2. Deliver packages 3. Return to Hub
class TripTracker:
    def __init__(self):
        self.__trips = [];

    def getLastTripTotalTimeTraveled(self):
        trip = self.__trips[-1];
        return trip["totalTimeTraveled"];
        
    def getTripTotalDistance(self):
        total = 0.0;
        
        for trip in self.__trips:
            total += trip["totalDistanceTraveled"];
            
        return total;    
        
    def getTripsSummaryString(self, truckID):
        logString = "";
    
        for i in range(len(self.__trips)):
            trip = self.__trips[i];
            hours = round(trip["totalTimeTraveled"] / (60 * 60), 2);
            miles = round(trip["totalDistanceTraveled"], 3);
            dt = trip["loadedDateTime"];
            finishedTime = trip["loadedDateTime"] + timedelta(days=0, seconds=trip["totalTimeTraveled"]);
    
            tripString = "\nTruck %d, Trip %d information\n------------------------------------\nDeparture time:\t\t%s\nFinished route time:\t%s\nDrive time:\t\t%s hours\nTotal distance:\t\t%s miles\n" % (truckID, i + 1, dt, finishedTime, hours, miles);

            logString += tripString;
        
        return logString;
        
    def addTrip(self, queue, time, distance, loadedDateTime):
        trip = {
            "visitedVerticesQueue": queue,
            "totalTimeTraveled": time,
            "totalDistanceTraveled": distance,
            "loadedDateTime": loadedDateTime
        };
        self.__trips.append(trip);
    
    # toString() function, useful for debugging
    def __str__(self):
        logString = "";
        
        for i in range(len(self.__trips)):
            trip = self.__trips[i];
            
            logString += ("Trip %d:\n" % (i) );
            logString += ("visitedVerticesQueue: %s\n" % ( trip["visitedVerticesQueue"] ) );
            logString += ("totalTimeTraveled: %f\n" % ( trip["totalTimeTraveled"] ) );
            logString += ("totalDistanceTraveled: %f\n" % ( trip["totalDistanceTraveled"] ) );
            logString += "\n";
                        
        return logString;    

class Truck:
    def __init__(self, truckID, driverID, startingVertex):
        self.__truckID = truckID; # 1, 2, or 3
        self.__driverID = driverID; # 1 or 2
        
        self.__startingVertex = startingVertex;
        self.__currentVertex = startingVertex; # Trucks start at the Hub
        
        self.__totalTimeTraveled = 0; # in seconds, integer
        self.__totalDistanceTraveled = 0; # in miles, float
        self.__startingTime = None;
        
        self.__visitedVertices = Queue();
        self.__visitedVertices.push(Node("4001 South 700 East", 0.0)); # Truck begins at the Hub
        self.__tripTracker = TripTracker();
        
        self.__loadedPackages = [];
        self.__verticesToVisit = [];
        self.__duplicateVertices = {}; # used to track if multiple packages are to be delivered to the same address, save trips

    # Time complexity: O(1)
    # Space complexity: O(1)        
    def loadPackage(self, package, loadedDateTime, delayExecution=-1):
    
        # delayExecution is used for time.sleep, make the test display slower for better simulation of time
        if delayExecution > 0:
            sleep(delayExecution); 
    
        # Instructions: "Each truck can carry a maximum of 16 packages"
        
        if len(self.__loadedPackages) < 16:
            self.__loadedPackages.append(package);
        else:
            raise ValueError("Truck %d cannot hold more than 16 packages." % self.__truckID);

        if package.deliveredTime != None: # package was already delivered. Error!
            raise ValueError("Package %d was already delivered at %s." % (package.packageID, package.deliveredTime) ); 

        self.__startingTime = loadedDateTime;

        self.__verticesToVisit.append(package.vertex.vertexID);

        # { "VERTEX_ID": ["PACKAGE_ID_01", ...] }        
        if package.vertex.vertexID in self.__duplicateVertices:
            self.__duplicateVertices[package.vertex.vertexID].append(package);
        else:
            self.__duplicateVertices[package.vertex.vertexID] = [];
            self.__duplicateVertices[package.vertex.vertexID].append(package);

        # update internal package variables
        package.setTruckID(self.__truckID);
        package.setDriverID(self.__driverID);
        package.setStatus(Package.EN_ROUTE);
        package.setLoadedTime(loadedDateTime);

        # logging message
        # str(...).rjust() is used to left pad zeroes "000XXX"
        print("Package %s loaded on Truck %d at %s" % (str(package.packageID).rjust(2, "0"), self.__truckID, package.loadedTime) );
    
    # Time complexity: O(1)
    def getPackagesAtVertex(self, vertexID):        
        return self.__duplicateVertices[vertexID];
    
    def getCurrentVertex(self):
        return self.__currentVertex;

    def getTruckID(self):
        return self.__truckID

    def getTotalDistanceTraveled(self):
        return self.__totalDistanceTraveled;
        
    def getLastTripTotalTimeTraveled(self):
        return self.__tripTracker.getLastTripTotalTimeTraveled();
        
    def getTripTotalDistance(self):
        return self.__tripTracker.getTripTotalDistance();
        
    def getTripsSummaryString(self):
        return self.__tripTracker.getTripsSummaryString(self.__truckID);
    
    # Time complexity: O(1)
    def getVerticesToVisit(self):
        return self.__verticesToVisit;

    # Time complexity: O(5) -> O(1)
    # Space complexity O(1)        
    def deliverPackage(self, package, distance, delayExecution=-1):
        # delayExecution is used for time.sleep, make the test display slower for better simulation of time
        if delayExecution > 0:
            sleep(delayExecution);     
    
        deliveryDateTime = None;    

        # check if the truck is delivering multiple packages to the same address
        # if this is the case, no extra distance is traveled
        if self.__currentVertex.vertexID != package.vertex.vertexID:
        
            self.__totalDistanceTraveled += distance;
            self.__totalTimeTraveled += self._calculateTimeFromDistance(distance);

            # update internal package variables
            package.setDeliveredTimeDelta( int(self.__totalTimeTraveled) ); # O(1)
            package.setStatus(Package.DELIVERED); # O(1)        
            
            self.__visitedVertices.push(Node(package.address, distance)); # O(1)
            self.__loadedPackages.remove(package); # O(1)
            self.__verticesToVisit.remove(package.vertex.vertexID); # O(1)

            # logging message
            # str(...).rjust() is used to left pad zeroes "000XXX"        
            print("Package %s delivered by Truck %d at %s, distance = %f miles, address = '%s'" % (str(package.packageID).rjust(2, "0"), self.__truckID, package.deliveredTime, distance, package.address) );

            self.__currentVertex = package.vertex; # update truck location
            
            # set the return value to the last delivered package
            deliveryDateTime = package.getDeliveredTime(); # O(1)
        
        else: # same vertex, not moving

            distance = 0.0;
            self.__totalDistanceTraveled += 0;
            self.__totalTimeTraveled += 0;

            # update internal package variables
            package.setDeliveredTimeDelta( int(self.__totalTimeTraveled) ); # O(1)
            package.setStatus(Package.DELIVERED); # O(1)        
            
            self.__visitedVertices.push(Node(package.address, distance)); # O(1)
            self.__loadedPackages.remove(package); # O(1)
            self.__verticesToVisit.remove(package.vertex.vertexID); # O(1)

            # logging message
            # str(...).rjust() is used to left pad zeroes "000XXX"        
            print("Package %s delivered by Truck %d at %s, distance = %f miles, address = '%s'" % (str(package.packageID).rjust(2, "0"), self.__truckID, package.deliveredTime, distance, package.address) );

            self.__currentVertex = package.vertex; # update truck location
            
            # set the return value to the last delivered package
            deliveryDateTime = package.getDeliveredTime(); # O(1)            
                
        # Check if all packages have been delivered
        if len(self.__verticesToVisit) == 0:
            print("\nAll loaded packages delivered by Truck %d." % (self.__truckID) );
            
        return deliveryDateTime;

    def _calculateTimeFromDistance(self, distance):
        # "Trucks travel at an average speed of 18 miles per hour"
        # 18 miles / 60 minutes = 0.3 miles per minute
        # 18 miles / 60 minutes * 60 seconds = 0.005 miles per second
        return int(distance / 0.005); # integer, units: seconds

    # Time complexity: O(3) -> O(1)
    # Space complexity: O(1)
    def returnToHub(self, startingVertex, distance, delayExecution=-1):        
        # delayExecution is used for time.sleep, make the test display slower for better simulation of time
        if delayExecution > 0:
            sleep(delayExecution);         
    
        self.__totalDistanceTraveled += distance;
        self.__totalTimeTraveled += self._calculateTimeFromDistance(distance);    

        print("Truck %d returning to hub, distance: %f miles.\n" % (self.__truckID, distance) );

        self.__visitedVertices.push(Node(startingVertex.address, distance)); # O(1)
        self.__currentVertex = startingVertex;

        totalTime = self.__totalTimeTraveled; # save this before being reset, for return value

        # log truck trip        
        self.__tripTracker.addTrip(self.__visitedVertices, self.__totalTimeTraveled, self.__totalDistanceTraveled, self.__startingTime); # O(1)

        # reset counter variables
        self.__startingTime = None;
        self.__totalDistanceTraveled = 0;
        self.__totalTimeTraveled = 0;
        self.__visitedVertices = Queue();
        self.__visitedVertices.push(Node("4001 South 700 East", 0.0)); # Truck begins at the Hub, O(1)        
        
        return totalTime;
        
    def getTraveledRoute(self):
        return str(self.__tripTracker);
