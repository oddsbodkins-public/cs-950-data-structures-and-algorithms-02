class Node:
    def __init__(self, label, distance=0):
        self.label = label;
        self.distance = distance;
        self.next = None;

class Queue:
    def __init__(self):
        self.__list = LinkedList();

    # Time complexity: O(1)
    # Space complexity: O(1)         
    def push(self, newItem):
        self.__list.append(newItem); # Insert as list tail (end of queue)

    # Time complexity: O(2) -> O(1)
    # Space complexity: O(1)    
    def pop(self):
        poppedItem = self.__list.getHead(); # Copy list head (front of queue)
        self.__list.removeAfter(None); # Remove list head
        
        return poppedItem; # Return popped item
        
    # toString() function, useful for debugging
    def __str__(self):
    
        logString = "";
        
        # start at the head and go to the tail
        node = self.__list.getHead();
        
        # boolean flag used in while loop
        firstItem = True;
        
        while (node != None):
            if firstItem == True:
                logString += ("'%s'" % (node.label) );            
                firstItem = False;
            else:
                logString += (" -> '%s'" % (node.label) );
            node = node.next;
            
        return logString;

class LinkedList:
    def __init__(self):
        self.__head = None;
        self.__tail = None;
    
    
    # Time complexity: O(1)
    # Space complexity: O(1)  
    def getHead(self):
        return self.__head;

    # Time complexity: O(1)
    # Space complexity: O(1)     
    def getTail(self):
        return self.__tail;

    # Time complexity: O(1)
    # Space complexity: O(1) 
    def append(self, newNode):
   
        if self.__head == None:
            self.__head = newNode;
            self.__tail = newNode;
         
        else:
            self.__tail.next = newNode;
            self.__tail = newNode;

    # Time complexity: O(1)
    # Space complexity: O(1) 
    def prepend(self, newNode):
   
        if self.__head == None:
            self.__head = newNode;
            self.__tail = newNode;
         
        else:
            newNode.next = self.__head;
            self.__head = newNode;

    # Time complexity: O(1)
    # Space complexity: O(1) 
    def insertAfter(self, currentNode, newNode):

        if self.__head == None:
            self.__head = newNode;
            self.__tail = newNode;
         
        elif currentNode is self.__tail:
            self.__tail.next = newNode;
            self.__tail = newNode;
         
        else:
            newNode.next = currentNode.next;
            currentNode.next = newNode;

    # Time complexity: O(1)
    # Space complexity: O(1)    
    def removeAfter(self, currentNode):
        
        # Special case, remove head
        if ( (currentNode == None) and (self.__head != None) ):
            nextNode = self.__head.next;
            self.__head = nextNode;
        
            if nextNode == None: # Removed last item
                self.__tail = None;
           
        elif currentNode.next != None:
            nextNode = currentNode.next.next;
            currentNode.next = nextNode;
        
            if nextNode == None: # Removed tail
                self.__tail = currentNode;
