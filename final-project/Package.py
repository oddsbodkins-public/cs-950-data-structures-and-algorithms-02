from datetime import timezone, date, datetime, timedelta;
import csv;

from MidSquareHashTable import MidSquareHashTable;

class Package:

    # package status 'constants'

    AT_THE_HUB  = 1;
    EN_ROUTE    = 2;
    DELIVERED   = 3;

    # delivery deadline status 'constants', or use 'None'?

    DELIVER_BY_END_OF_DAY   = 1;
    DELIVER_BY_SET_TIME     = 2;    
    
    def __init__(self, packageID, address, city, state, postalCode, deadlineTime, massInKilograms, specialInstructions):
        self.packageID = int(packageID);
        self.address = address;
        self.city = city;
        self.state = state;
        self.postalCode = int(postalCode);
        self.vertex = None;

        # helper variables for preparing datetime objects
        nowDT = datetime.utcnow().replace(tzinfo=timezone.utc);
        todayDateString = nowDT.strftime("%Y-%m-%d");       
        
        if (deadlineTime == "EOD"):
            self.deadlineStatus = self.DELIVER_BY_END_OF_DAY; 
            self.deadlineDateTime = datetime.strptime("%s 23:59:59" % (todayDateString), "%Y-%m-%d %H:%M:%S" );
        else:
            self.deadlineStatus = self.DELIVER_BY_SET_TIME;
            self.deadlineDateTime = datetime.strptime("%s %s" % (todayDateString, deadlineTime), "%Y-%m-%d %I:%M:%S %p" )
        
        
        self.massInKilograms = int(massInKilograms);
        self.specialInstructions = specialInstructions;
        
        self.status = self.AT_THE_HUB;
        self.loadedTime = None;
        self.deliveredTime = None;
        self.truckID = None;
        self.driverID = None;

    def setLocation(self, address, city, state, postalCode):
        self.address = address;
        self.city = city;
        self.state = state;
        self.postalCode = int(postalCode);        
        
    def setVertex(self, vertex):
        self.vertex = vertex;
        
    def setTruckID(self, truckID):
        self.truckID = truckID;
        
    def setDriverID(self, driverID):
        self.driverID = driverID;

    def setLoadedTime(self, loadedTime):
        self.loadedTime = loadedTime;

    # Set the delivered time with the number of seconds since the loaded time
    def setDeliveredTimeDelta(self, numberOfSeconds):
        self.deliveredTime = self.loadedTime + timedelta(days=0, seconds=numberOfSeconds);
        
    def getDeliveredTime(self):
        return self.deliveredTime;
    
    def setStatus(self, status):
        self.status = status;
    
    # Method used by HashMap to compare when searching for key
    def key(self):
        return self.packageID
    
    def getStatusString(self, dateTime):
        if dateTime < self.loadedTime:
            return "At the hub";
        if dateTime >= self.loadedTime and dateTime < self.deliveredTime:
            return "En Route";
        if dateTime >= self.deliveredTime:
            return "Delivered";
            
        return "";
    
    def getSpecialInstructionString(self):
        if len(self.specialInstructions) == 0:
            return "False";
        else:
            return "True ";
    
    def getPackageStringAtDateTime(self, dateTime):
        return ("Package id: %s deadline: %s special-instructions: '%s' truck: %d driver: '%s' loaded-time: %s delivered-time: %s status: '%s' " % (str(self.packageID).rjust(2, "0"), self.deadlineDateTime, self.getSpecialInstructionString(), self.truckID, self.driverID, self.loadedTime, self.deliveredTime, self.getStatusString(dateTime)) );    
    
    # toString() function, useful for debugging
    def __str__(self):        
        return ("id: %s deadline: %s mass: %s instructions: '%s' truck: %d loaded-time: %s delivered-time: %s status: '%s' " % (str(self.packageID).rjust(2, "0"), self.deadlineDateTime, str(self.massInKilograms).rjust(3, "0") + " kilos", self.getSpecialInstructionString(), self.truckID, self.loadedTime, self.deliveredTime, self.getStatusString()) );
        
class PackageHelper:

    def __init__(self, csvFilePath):
        self.__csvFilePath = csvFilePath;
        self.hashMap = None;
        self.__loadCSVFile();
        
    # Time complexity: O(2n) -> O(n), n=number of lines in CSV file
    # Space complexity: O(n^2), n=number of lines in CSV file, list inside of a list
    def __loadCSVFile(self):
        # CSV Columns for reference
        # ['ID', 'Address', 'City', 'State', 'Zip', 'Deadline', 'Mass (kg)', 'Special Notes']

        with open(self.__csvFilePath) as packageCSVFile:
            csvReader = csv.reader(packageCSVFile, delimiter=",", quotechar="\"");

            i = 0; # variable to track the line number in the loop
            
            for row in csvReader:
                i += 1;
                
            packageCSVFile.seek(0); # reset the file stream pointer to the beginning
            
            self.hashMap = MidSquareHashTable(i); # create the HashMap
            i = 0; # reset the line tracker      

            for row in csvReader:
            
                if i == 0: # skip the first row, column-names in CSV file
                    i += 1;
                    continue;
                
                p = Package(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]);
                self.hashMap.insert(p);
                
                i += 1;

    # Time complexity: O(n^2 * m), n = size of HashMap, m = number of vertices from address list  
    # Space complexity: O(n^2), n=number of lines in CSV file, list inside of a list                
    def updatePackageVertices(self, vertexHelper):
        for i in range(self.hashMap.getNumberOfItems() + 1):
            if i > 0: # minimum package id is 1
            
                p = self.hashMap.search(i); # O(n)
        
                v = vertexHelper.addressSearch(p.address); # O(m)
                p.setVertex(v);
                
