import csv;
from Vertex import Vertex;    

class DistanceHelper:
    def __init__(self, csvFilePath):
        self.__distanceTable = [];
        self.__csvFilePath = csvFilePath;
        self.__loadDistanceTable();
    
    # Load the CSV file and populate the distance list
    # Time complexity: O(n^2), n=CSV row length or column length, row length = column length
    # Space complexity: O(n^2)
    def __loadDistanceTable(self):
           
        # The destination table was modified. The addresses and titles were replaced with numerical ids for simplicity.
        # A separate CSV file is used for mapping the id to an address.
        
        # CSV Columns for reference
        # -,1,2,3...27
        # 1,
        # 2,
        # 3,
        # ...

        # store the CSV in a two-dimensional array / Python list inside a list
        with open(self.__csvFilePath) as distanceTableCSVFile:
            csvReader = csv.reader(distanceTableCSVFile, delimiter=",", quotechar="\"");

            # get the number of items, we need the size for the HashMap
            for row in csvReader:
                horizontalList = [];
                
                for column in row:
                    if (column == "-" or column == ""):
                        column = -1; # -1 represents an empty column that needs to be filled, the table is only half-filled
                    
                    horizontalList.append(float(column)); # cast strings to float

                self.__distanceTable.append(horizontalList);       
    
        # This algorithm could be used to populate empty entries in the distance table.
        # After trial and error, I realized that it is unneccessary, since a lookup function that reverses the starting point and destination point works just as well.  
        #
        #i = len(self.__distanceTable) - 1;
        #while i > 0:
        #    j = i;
        #    
        #    while j < len(self.__distanceTable[i]) - 1:
        #        # print("j: %d, j + 1: %d" % (j, j + 1));
        #        self.__distanceTable[i][j + 1] = self.__distanceTable[j + 1][i];
        #        j += 1; 
        #    
        #    i -= 1;    
    
    # Time complexity: O(1), Python List lookup - https://wiki.python.org/moin/TimeComplexity
    # Space complexity: O(1)
    def getDistance(self, fromVertexID, toVertexID):
            # type check and bounds check
            if not isinstance(fromVertexID, int) or fromVertexID < 0 or fromVertexID > len(self.__distanceTable):
                return None;

            if not isinstance(toVertexID, int) or toVertexID < 0 or toVertexID > len(self.__distanceTable):
                return None;
                
            distance = self.__distanceTable[fromVertexID][toVertexID];
            
            # Invalid distance, the table is not entirely populated
            # The distance is the same from A -> B as B -> A, use the reverse path
            if distance == -1:
                distance = self.__distanceTable[toVertexID][fromVertexID];
                            
            return distance;
