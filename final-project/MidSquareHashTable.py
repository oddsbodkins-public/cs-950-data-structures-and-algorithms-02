import math;

# https://docs.python.org/3/tutorial/classes.html#private-variables, convention is two underscores are private variables, functions, etc.

# This is a Chaining Hash Map.
# Objects stored in this HashMap must implement a 'key()' function.

class MidSquareHashTable:

    # Time complexity: O(n) n=requested size
    # Space complexity: O(n^2), list inside of a list
    def __init__(self, size):
        self.__hashTable = [];
        self.__numberOfItemsInTable = 0;
        
        for i in range(size): # create list of length 'size', with inner empty list values
            self.__hashTable.append([]);

    # C language implementation of mid-square hash
    # https://gist.github.com/urwithajit9/e9b899ac4d9b73fa88be818047a58d19
    
    # Helper function for mid-square hash. Given an integer count the number of digits. Ex: 12345 -> 5
    # Time complexity: O(n) n = length of integer
    def __countIntegerDigits(self, key):
        count = 0;
        
        while key != 0:
            key = int(key / 10); # cast to integer, we want to count the ones place, tens place, hundreds place, ...

            count += 1;
            
        return count;   

    # Helper function for mid-square hash. Ex:  getIntegerNthDigit(928347892, 5) -> 4     
    def __getIntegerNthDigit(self, number, position):
        # position is counted from the left, first digit is highest number place
        digit = 0;
        fraction = 0;
        length = self.__countIntegerDigits(number);
        
        if length - position < 0: # invalid arguments, number must be positive
            return 0;
            
        fraction =  int(number / math.pow(10, (length - position)));
        digit = (fraction % 10);
        
        return digit;
        
    # Helper function for mid-square hash. Similiar to getIntegerNthDigit(), but returns multiple numbers. Ex: getIntegerDigitsGroup(9237429, 3, 3) -> 374
    # Time complexity: O(n + m) -> O(n)
    def __getIntegerDigitsGroup(self, number, startingPosition, count):
        # position is counted from the left, first digit is highest number place
        digit = 0;
        finalNumber = 0;
        length = self.__countIntegerDigits(number); # O(n)

        if ((startingPosition + count) - 1) <= length:
            for i in range(count): # O(m)
            
                digit = self.__getIntegerNthDigit(number, startingPosition);
                startingPosition += 1;
                
                finalNumber = finalNumber * 10;
                finalNumber = finalNumber + digit;
                
        return finalNumber;

    # Using the Mid-square hash
    # https://learn.zybooks.com/zybook/WGUC950AY20182019/chapter/7/section/6
    # Time complexity: O(n + m) -> O(n)
    def __midSquareHash(self, key):
        hashValue = 0;
        midPosition = 0;
        
        if key == 0:
            raise ValueError("Invalid mid-square hash key! Must be greater than 0!");
        
        keySquare = int(key * key);
        keySquareLength = self.__countIntegerDigits(keySquare); # O(n)
        midPosition = int(keySquareLength / 2);

        # Compute the number of digits to use from the key's middle.
        # log [base 10] of key, take the ceiling (round up to integer).
        # Example size: 200 = log(200) = 7.6439 -> ceiling(7.6439) = 8, 8 / 2 = 4 digits.
        extractNDigits = math.ceil(math.log(key));
        extractNDigits = int(extractNDigits / 2);
        
        hashValue = self.__getIntegerDigitsGroup(keySquare, midPosition, extractNDigits); # O(m)

        return hashValue;
    
    # insert a new item into the hash table
    # https://learn.zybooks.com/zybook/WGUC950AY20182019/chapter/7/section/8
    # Time complexity: O(n)
    # Space complexity: O(n * m), n = existing list size, m = inner list size, list inside of a list
    def insert(self, item):
        # check if the argument has a function called key()
        if not hasattr(item.key, '__call__'):
            return;        

        # the hash table is a list of lists (two-dimensional array), get the index
        bucketIndex = self.__midSquareHash(item.key()) % len(self.__hashTable); # O(n)
        outerBucketContainer = self.__hashTable[bucketIndex]; # O(1)
        
        # append the value to the inner list
        outerBucketContainer.append(item); # O(1)
        self.__numberOfItemsInTable += 1;

    # Time complexity: O(2n) -> O(n)
    # Space complexity: O(n * m - 1), n = existing list size, m = inner list size, list inside of a list
    def remove(self, key):
        # check if the argument is a integer
        if not isinstance(key, int):
            return;        

        # the hash table is a list of lists (two-dimensional array), get the index
        bucketIndex = self.__midSquareHash(key) % len(self.__hashTable); # O(n)
        outerBucketContainer = self.__hashTable[bucketIndex]; # O(1)

        # search for the item 
        for item in outerBucketContainer: # O(n)
            if item.key() == key:
                outerBucketContainer.remove(item); # delete the item from the inner list

    # Search for a matching key
    # Returns the item if found, or None if not found
    # Time complexity: O(2n) -> O(n)
    def search(self, key):
        # check if the argument is a integer
        if not isinstance(key, int):
            return;    

        # the hash table is a list of lists (two-dimensional array), get the index
        bucketIndex = self.__midSquareHash(key) % len(self.__hashTable); # O(n)
        outerBucketContainer = self.__hashTable[bucketIndex]; # O(1)

        # search for the item 
        for item in outerBucketContainer: # O(n)
            if item.key() == key:
                return item;
        
        return None; # item not found
        
    def getNumberOfItems(self):
        return self.__numberOfItemsInTable;
        
    # Useful for debugging
    # toString() function
    # Representation of the entire hash table. Each bucket is shown as a pointer to a list object.
    def __str__(self):
        index = 0;
        s =  "   --------\n";
        for item in self.__hashTable:
            s += ("%2d:|   ---|-->%s\n" % (index, str(item)) );
            index += 1;
        s += "   --------";
        return s;
