# Heuristic algorithm - A technique that willingly accepts a non-optimal or less accurate solution in order to improve execution speed

from operator import attrgetter

class Item:
    def __init__(self, weight, value):
        self.weight = weight;
        self.value = value;

class Knapsack:
    def __init__(self, weightLimit):
        self.weightLimit = weightLimit;
        self.currentWeight = 0;
        self.totalValue = 0;
        self.itemList = [];       

    def heuristicAlgorithm01(self, availableItems):
        # sort the item list in descending order, most valuable item at itemList[0] ...
        availableItems.sort(key = attrgetter('value'), reverse = True);
        
        for item in availableItems:
            if self.currentWeight + item.weight <= self.weightLimit:
                self.itemList.append(item);
                self.currentWeight += item.weight;
                self.totalValue += item.value;
                
    def printItemList(self):
        i = 0;
        print("List of items\n-------------------");
        for item in self.itemList:
            print("Item[{}]: weight: {} value: {}".format(i, item.weight, item.value));    
            i += 1;             
        print("---------------------");
        print("Total weight in knapsack: {}".format(self.currentWeight));
        print("Total value in knapsack: {}".format(self.totalValue));            

        
def main():
    availableItems = [];
    availableItems.append(Item(18, 95));
    availableItems.append(Item(12, 60));
    availableItems.append(Item(8, 42));
    availableItems.append(Item(6, 25));
    
    # get user input
    weightLimit = int(input("Please enter a maximum knapsack weight: "));    
    k = Knapsack(weightLimit);
    
    k.heuristicAlgorithm01(availableItems);
    k.printItemList();
    
main();
    

