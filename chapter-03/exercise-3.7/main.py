import math

# Point class
class Point:
    def __init__(self, x, y):
        self.x = x;
        self.y = y;

def getDistanceBetweenPoints(p1, p2):
    
    # d = sqrt((x_p - x_1)^2 + (y_p - y_1)^2)
    distance = math.sqrt( ((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)) );
    return distance;
        

def main():
    destinationPoint = Point(0, 0);
    previousPoint = Point(0, 0);
    currentPoint = Point(0, 0);

    print("User input\n------------");

    # Read in x and y for Point P
    print("1. Define an instance of class 'Point'.");
    destinationPoint.x = int(input("Type p.x: "));
    destinationPoint.y = int(input("Type p.y: "));

    distanceToDestination = getDistanceBetweenPoints(currentPoint, destinationPoint);
    smallestDistanceToDestination = distanceToDestination

    # Read in num of steps to be taken in X and Y directions
    print("\n2. Type the number of steps to take along the x-axis and y-axis each iteration.");
    stepsAlongXAxis = int(input("Type the number of steps for x: "));
    stepsAlongYAxis = int(input("Type the number of steps for y: "));

    # Read in num of steps to be taken (backwards) every 3 steps
    print("\n3. Type the number of steps to take backwards along both axes every third iteration.");
    backwardSteps = int(input("Type the number of steps to take backwards. (Must be smaller than input from #2): "));
    
    print("\n------------------\nStarting at the origin (0, 0) ...");
    
    endLoop = False; i = 1;
    while endLoop != True:
        # save current state to previous
        previousPoint = currentPoint;
    
        # add steps
        currentPoint.x += stepsAlongXAxis;
        currentPoint.y += stepsAlongYAxis;
    
        # calculate the new distance
        distanceToDestination = getDistanceBetweenPoints(currentPoint, destinationPoint);

        if distanceToDestination < smallestDistanceToDestination:
            smallestDistanceToDestination = distanceToDestination
    
        # do the backwards step
        if i % 3 == 0:
            currentPoint.x -= backwardSteps;
            currentPoint.y -= backwardSteps;
            
            # calculate the distance again
            distanceToDestination = getDistanceBetweenPoints(currentPoint, destinationPoint);
            
        i += 1;
        endLoop = True;
    
    # Example input: 4, 5, 2, 3, 1

    # Expected output
    """
Point P: (4,5)
Arrival point: (4,6)
Distance between P and arrival: 1.000000
Number of iterations: 2    
    """
    
main();
