# Greedy algorithm - A greedy algorithm solves a problem by assuming that the optimal choice at a given moment during the algorithm will also be the optimal choice overall

from operator import attrgetter, methodcaller

class Item:
    def __init__(self, weight, value):
        self.weight = weight;
        self.value = value;
        self.fraction = 1.0;
        
    def valueToWeightRatio(self):
        return self.value / self.weight;        

class Knapsack:
    def __init__(self, weightLimit):
        self.weightLimit = weightLimit;
        self.currentWeight = 0;
        self.totalValue = 0;
        self.itemList = [];       

    def heuristicAlgorithm01(self, availableItems):
        # sort the item list in descending order, most valuable item at itemList[0] ...
        availableItems.sort(key = attrgetter('value'), reverse = True);
        
        for item in availableItems:
            if self.currentWeight + item.weight <= self.weightLimit:
                self.itemList.append(item);
                self.currentWeight += item.weight;
                self.totalValue += item.value;

    def greedyAlgorithm01(self, availableItems):
        # sort the item list in descending order, most valuable item ration at itemList[0], example: $8 at 2 pounds = $4 per pound, compare price per pound
        availableItems.sort(key = methodcaller('valueToWeightRatio'), reverse = True);
        
        for item in availableItems:
            # Check if the whole item will fit in the knapsack
            if self.currentWeight + item.weight <= self.weightLimit:
                item.fraction = 1.0;
                self.itemList.append(item);
                self.currentWeight += item.weight;
                self.totalValue += item.value;
            # Only a part of the item will fit
            else:
                if self.currentWeight == self.weightLimit:
                    return;
                                        
                item.fraction = (self.weightLimit - self.currentWeight) / item.weight;
                self.itemList.append(item);
                self.currentWeight += item.fraction * item.weight;
                self.totalValue += item.fraction * item.value;
                break;
                
    def printItemList(self):
        i = 0;
        print("List of items\n-------------------");
        for item in self.itemList:
            print("Item[{}]: Original weight: {}, fractional weight: {}, Original value: {}, fractional value: {}, fraction: {}".format(i, item.weight, item.weight * item.fraction, item.value, item.value * item.fraction, item.fraction));    
            i += 1;             
        print("---------------------");
        print("Total weight in knapsack: {}".format(self.currentWeight));
        print("Total value in knapsack: {}".format(self.totalValue));            

        
def main():
    availableItems = [];
    availableItems.append(Item(8, 42));    
    availableItems.append(Item(18, 95));
    availableItems.append(Item(12, 60));
    availableItems.append(Item(6, 25));
    
    # get user input
    weightLimit = int(input("Please enter a maximum knapsack weight: "));    
    k = Knapsack(weightLimit);
    
    k.greedyAlgorithm01(availableItems);
    k.printItemList();
    
main();
    

