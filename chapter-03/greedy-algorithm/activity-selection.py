from operator import attrgetter;

class Activity:
    # startTime and finishTime are integers, representing minutes, in 24 hour time, so 1 hour = 60 minutes. Example: 06:00 PM = 1800 (18:00) 
    def __init__(self, name, startTime, endTime):
        self.name = name
        self.startTime = startTime
        self.endTime = endTime

def printActivityList(activityList):
    i = 0;

    for activity in activityList:
        print("Activity[{}]: name: '{}' start: '{}' finish: '{}'".format(i, activity.name, activity.startTime / 60, activity.endTime / 60));
        i += 1;

def greedyActivitySelectionAlgorithm01(allActivities):
    selectedActivities = [];
    # Sort the activities based on the end time, ascending, from earliest to latest
    allActivities.sort(key = attrgetter('endTime'));
    
    # Add the first event
    selectedActivities.append(allActivities[0]);
        
    for activity in allActivities:
        if activity.startTime >= selectedActivities[-1].endTime:
            selectedActivities.append(activity);

    printActivityList(selectedActivities);    

        
def main():
    activityList = [];
    activityList.append(Activity("History museum tour", 9 * 60, 10 * 60));
    activityList.append(Activity("Fireworks show", 20 * 60, 21 * 60));
    activityList.append(Activity("Snorkeling", 16 * 60, 17 * 60));
    activityList.append(Activity("Hang gliding", 14 * 60, 16 * 60));
    activityList.append(Activity("Morning mountain hike", 9 * 60, 12 * 60));
    activityList.append(Activity("Day mountain hike", 13 * 60, 16 * 60));
    activityList.append(Activity("Night movie", 19 * 60, 21 * 60));
    activityList.append(Activity("Boat tour", 11 * 60, 14 * 60));
    
    greedyActivitySelectionAlgorithm01(activityList);
    
main();
