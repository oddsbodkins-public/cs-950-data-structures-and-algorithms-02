# Dynamic programming is a problem solving technique that splits a problem into smaller subproblems, computes and stores solutions to subproblems in memory, and then uses the stored solutions to solve the larger problem. 

def printStringMatrix(twoDimensionalList):
    height = len(twoDimensionalList);
    width = len(twoDimensionalList[0]);

    print("Width: {} Height: {}".format(width, height));
    
    for row in range(height):
        for column in range(width):
        
            print(twoDimensionalList[row][column], end=''); # do not print a newline "\n"

            if column < (width - 1): # print comma for each column, except the last
                print(",", end='');
        
        print(""); # print newline for end of row

# Time complexity - O ( len(string01) * len(string02) )
# Space complexity - O ( len(string01) * len(string02) ) 
# Longer string should be in the second argument, easier to read / debug

def longestCommonSubstring(string01, string02):
    # make the algorithm case-insensitve, modify string
    string01 = string01.lower();
    string02 = string02.lower();    

    width = len(string02);
    height = len(string01); 
        
    # Create a two-dimensional list (array), not straightforward in Python
    # https://stackoverflow.com/questions/6667201/how-to-define-a-two-dimensional-array

    matrix = [[0 for x in range(width)] for y in range(height)];

    maxValue = 0;
    maxValueRow = 0;
    maxValueColumn = 0;
        
    for row in range(height):
        for column in range(width):

            if string01[row] == string02[column]:
                # Get the value in the cell that's up and to the left, or 0 if no such cell
                upLeft = 0;
                
                if row > 0 and column > 0:
                    upLeft = matrix[row - 1][column - 1];
                
                matrix[row][column] = 1 + upLeft;

                if matrix[row][column] > maxValue:
                    maxValue = matrix[row][column];
                    maxValueRow = row;
                    maxValueColumn = column;            
                
            else:
                matrix[row][column] = 0;

    # debug matrix                
    printStringMatrix(matrix);
    print("maxValue: {}, row: {}, column: {}".format(maxValue, maxValueRow, maxValueColumn));
    
    return string01[(maxValueRow + 1) - maxValue : maxValueRow + 1];

def main():
    s = longestCommonSubstring("embrace", "braille");
    print("\nLongest common substring: '{}'".format(s));


# Uses less space (memory / RAM)
def longestCommonSubstringOptimized(str1, str2):
    # Create one row of the matrix.
    matrix_row = [0] * len(str2)

    # Variables to remember the largest value, and the row it
    # occurred at.
    max_value = 0
    max_value_row = 0
    for row in range(len(str1)):
        # Variable to hold the upper-left value from the
        # current matrix position.
        up_left = 0
        for col in range(len(str2)):
            # Save the current cell's value; this will be up_left
            # for the next iteration.
            saved_current = matrix_row[col]
        
            # Check if the characters match
            if str1[row] == str2[col]:
                matrix_row[col] = 1 + up_left
                
                # Update the saved maximum value and row,
                # if appropriate.
                if matrix_row[col] > max_value:
                    max_value = matrix_row[col]
                    max_value_row = row
            else:
                matrix_row[col] = 0
                
            # Update the up_left variable
            up_left = saved_current

    # The longest common substring is the substring
    # in str1 from index max_value_row - max_value + 1, 
    # up to and including max_value_row.
    start_index = max_value_row - max_value + 1
    return str1[start_index : max_value_row + 1]



main();
