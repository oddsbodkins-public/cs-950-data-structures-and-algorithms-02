#! /usr/bin/python3

print("Hello, world!");
print("This" + " is " + "a " + "test");

# This is a comment.
# '//' and '/* */' do not work in Python!
 
# You can cheat and use multiline strings as multiline comments
"""
ABBC
ASDLFK
"""
 
x = 5; # Wow! Inline comment!
y = "Hello, world!";

y = str(5); # casting
a = int(4.2);
b = float(3.14);

if x == 5:
    print("X is 5!");

print(y);
print(a);
print(b);

print(type(b)); # print the type of the variable
print(type(y));

# single or double quotes are allowed for strings
foo = 'foo'
bar = "bar"

print(foo);
print(bar);
