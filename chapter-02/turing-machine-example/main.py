# From C950 Zybook, Section 2.10

from turing_machine import TuringMachine

alphabet =['a','b', '*']

print('Enter a string using the following symbols:', end=' ')
for i in alphabet:
    print(i, end=' ')
print()
input_string = input()

# Make sure input characters are valid (in the alphabet)
for char in input_string:
    if char not in alphabet:
        print('Invalid input string.')
        exit()


initial_state = 'q_0'
final_states = ['q_acc', 'q_rej']

# Transition function
# This machine accepts input if there are two 'b's, order does not matter
transition_function = {('q_0','a'):('q_0', 'a', 'R'),
                       ('q_0','b'):('q_1', 'b', 'R'),
                       ('q_0','*'):('q_rej', '*', 'L'),
                       ('q_1','a'):('q_1', 'a', 'R'),
                       ('q_1','b'):('q_acc', 'b', 'R'),
                       ('q_1','*'):('q_rej', '*', 'L')}

turing = TuringMachine(alphabet, input_string, '*', initial_state, final_states,
                  transition_function)

# Step through Turing machine until a final state is reached
while not turing.final_state():
    turing.step()

if turing.current_state == 'q_acc':
    print('String %s is accepted.' % input_string)
else:
    print('String %s is rejected.' % input_string)
