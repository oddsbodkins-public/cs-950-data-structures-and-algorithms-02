def main():
    student_grades = {}  # Create an empty dict
    grade_prompt = "Enter name and grade (Ex. 'Bob A+'):"
    menu_prompt = ("1. Add/modify student grade\n"
                    "2. Delete student grade\n"
                    "3. Print student grades\n"
                    "4. Quit\n")

    while True:  # Exit when user enters no input
        command = input(menu_prompt).lower().strip()
        if command == '1':
            name, grade = input(grade_prompt).split()
            print("1");
        elif command == '2':
            print("2");
        elif command == '3':
            print("3");
        elif command == '4':
            break;
        else:
            print('Unrecognized command.');
    
def main2():
    my_dict = dict(bananas=1.59, fries=2.39, burger=3.50, sandwich=2.99); # valid alternate way of declaring dictionary

    my_dict2 = {
        "bananas": 1.59,
        "fries": 2.39,
        "burger": 3.50,
        "sandwich": 2.99
    }
    
    my_dict2.update(dict(soda=1.49, burger=3.69)) # merges two dictionaries, overwrites if duplicates found, keys from passed argument persist
    burger_price = my_dict2.get('burger', 0) # check if key in dictionary exists, if not returns 0
    print(burger_price);
    
    my_dict2['burger'] = my_dict2['sandwich']
    val = my_dict2.pop('sandwich')
    print(my_dict2['burger'])
    print(val);    
    
    
def main3():
    # items() function
    num_calories = dict(Coke=90, Coke_zero=0, Pepsi=94)

    for key, value in num_calories.items():
        print('%s: %d' % (key, value))

    # keys() function
    num_calories = dict(Coke=90, Coke_zero=0, Pepsi=94)
    for soda in num_calories.keys():
        print(soda)
        
    # items() function
    num_calories = dict(Coke=90, Coke_zero=0, Pepsi=94)
    for soda in num_calories.values():
        print(soda)

# Print the name and grade percentage of the student with the highest total points
# Find the average score of each assignment.
# Find and apply a curve to each student's total score, such that the best student has 100% of the total points

def main4():
    # student_grades contains scores (out of 100) for 5 assignments
    student_grades = {
        'Andrew': [56, 79, 90, 22, 50],
        'Colin': [88, 62, 68, 75, 78],
        'Alan': [95, 88, 92, 85, 85],
        'Mary': [76, 88, 85, 82, 90],
        'Tricia': [99, 92, 95, 89, 99]
    };
    
    student_totals = {};
    student_grades_as_key = {};
    for student, gradesList in student_grades.items():
        studentTotal = totalPossible = 0;
        
        for grade in gradesList:
            studentTotal += grade;
            totalPossible += 100;
        
        student_totals[student] = studentTotal / totalPossible;
        student_grades_as_key[ (studentTotal / totalPossible) ] = student;
        
        #print("%s: Grades: %d / %d = %f" % (student, studentTotal, totalPossible, studentTotal / totalPossible));
    
    studentList = list(student_totals.values());
    studentList.sort(reverse=True);
    
    studentWithHighestGrade = student_grades_as_key.get(studentList[0]);
    print("Student with highest grade: '%s': %f" % (studentWithHighestGrade, student_totals.get(studentWithHighestGrade)) );
main4();
